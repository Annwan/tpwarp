package annwan.tpwarps.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class CommandWarps implements CommandExecutor {
    private Map<String, Object> warps;

    public CommandWarps()
    {
        warps = new HashMap<>();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args)
    {
        boolean ret;
        if (sender instanceof Player) {
            Player player = (Player) sender;
            switch (cmd.getName()) {
                case "warps":
                    ListWarps(player);
                    ret = true;
                    break;
                case "setwarp":
                    AddWarp(player, args);
                    ret = true;
                    break;
                case "delwarp":
                    RmWarp(player, args);
                    ret=true;
                    break;
                case "warp":
                    GetToWarp(player, args);
                    ret = true;
                    break;
                default:
                    ret = false;
            }
        } else ret = false;
        return ret;
    }

    private void ListWarps(Player player)
    {
       String[] warpList = new String[]{};
       if (warps.isEmpty()) warpList = new String[]{"No defined warps"};
       else warpList = warps.keySet().toArray(warpList);
       player.sendMessage(warpList);
    }

    private void RmWarp(Player player, String[] args)
    {
        if (args.length == 0)
        {
            player.sendMessage("You must specify a name for the warp to remove");
        }
        else
        {
            String warpName = args[0];
            warps.remove(warpName);
            player.sendMessage("Warp §b" + warpName + "§r removed successfully");
        }
    }

    private void GetToWarp(Player player, String[] args)
    {
        if (args.length == 0)
        {
            player.sendMessage("You must specify the name of the warp");
        }
        else
        {
            String warpName = args[0];
            if (!warps.containsKey(warpName))
            {
                player.sendMessage("No such warp: §b" + warpName);
            } else {
                Location plLoc = player.getLocation();
                Location tpLoc = (Location) warps.getOrDefault(warpName, plLoc);
                player.teleport(tpLoc);
            }
        }
    }

    private void AddWarp(Player player, String[] args)
    {
        if (args.length == 0)
        {
            player.sendMessage("You must specify a name for the new warp");
        }
        else
        {
            Location loc = player.getLocation();
            String warpName = args[0];
            warps.put(warpName, loc);
            player.sendMessage("Warp §b" + warpName + "§r successfully added at §b" + loc.toString());
        }
    }



    public void WriteSave(FileConfiguration config) {
        config.set("warps", warps);
    }

    public void LoadSave(FileConfiguration config) {
        warps = config.getConfigurationSection("warps").getValues(false);
    }
}
