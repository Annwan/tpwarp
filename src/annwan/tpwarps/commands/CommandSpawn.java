package annwan.tpwarps.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player)
        {
            Player player = (Player) commandSender;
            Location worldSpawn = player.getLocation().getWorld().getSpawnLocation();
            player.teleport(worldSpawn);
        }
        return false;
    }
}
