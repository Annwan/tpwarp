package annwan.tpwarps;

import annwan.tpwarps.commands.CommandSpawn;
import annwan.tpwarps.commands.CommandWarps;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private CommandWarps cmdWarps;
    private FileConfiguration config;
    @Override
    public void onEnable() {

        config = getConfig();
        // /spawn
        getCommand("spawn")
                .setExecutor(new CommandSpawn());
        // warp command family
        cmdWarps = new CommandWarps();
        cmdWarps.LoadSave(config);
        getCommand("warp")
                .setExecutor(cmdWarps);
        getCommand("setwarp")
                .setExecutor(cmdWarps);
        getCommand("delwarp")
                .setExecutor(cmdWarps);
        getCommand("warps")
                .setExecutor(cmdWarps);
        // tp extension familly

    }

    @Override
    public void onDisable() {
        cmdWarps.WriteSave(config);
        saveConfig();
    }
}
